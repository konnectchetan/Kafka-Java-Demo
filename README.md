## Instructions 
- Switch to home directory
    
    ```cd /home/<user_name>```
- Get the code cloned
    
    ```git clone https://gitlab.com/konnectchetan/Kafka-Java-Demo/```
- Change directory to code

    ```cd Kafka-Java-Demo```

- If you wish to change the topic name, then you need to edit the controller and consumer config files (OPTIONAL)
    
    - Controller
    ```vi src/main/java/com/thinknyx/kafkademo2/controller/KafkaController.java```
    
    - Consumer Config 
    ```vi src/main/java/com/thinknyx/kafkademo2/consumer/MyTopicConsumer.java```

- Run the app using ```./mvnw spring-boot:run```
- Now access the application in browser
    - To produce message url will be

    ```your_vm_ip:8080/kafka/produce?message=SomeMessage```
    - To Consume message url will be 
     
     ```your_vm_ip:8080```
     or
     ```your_vm_ip:8080/consume```
