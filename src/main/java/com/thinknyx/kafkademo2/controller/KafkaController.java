package com.thinknyx.kafkademo2.controller;

import com.thinknyx.kafkademo2.consumer.MyTopicConsumer;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class KafkaController {

    private KafkaTemplate<String, String> template;
    private String TOPIC_NAME = "demo";
    private MyTopicConsumer consumer;
    public KafkaController(KafkaTemplate<String, String> template, MyTopicConsumer consumer) {
        this.template = template;
        this.consumer= consumer;
    }

    @GetMapping("/kafka/produce")
    public void produce(@RequestParam String message) {
        template.send(TOPIC_NAME, message);
    }

    @GetMapping("/consume")
    public List<String> getMessages(){
        return consumer.getMessages();
    }

    @GetMapping("/")
    public List<String> getMessages2(){
        return consumer.getMessages();
    }
}
